/* Emily Beauto
 * CS 4280
 * Project 4 Target ASM
 * 12/13/22
 * staticSem.h
 */

#ifndef STATICSEM1_H
#define STATICSEM1_H
#include "parser.h"
#include "node.h"
#include "token.h"
using namespace std;


void staticSem(node*, int countScope, int varCount, FILE *out, string output);
#endif
