/* Emily Beauto
 * CS 4280
 * Project 4 Target ASM
 * 12/13/22
 * token.h
 */

#ifndef TOKEN_H
#define TOKEN_H
#include <string>
#include <iostream>

using namespace std;

//all token ID types + error + falseid
enum tokenType {
	EOFtk, IDtk, INTtk, ASSIGNtk, COMPAREtk, LESSTHANEQtk, GREATERTHANEQtk,
	COLONtk, INCREMENTtk, DECREMENTtk, MULTIPLYtk, DIVIDEtk, MODULUStk, 
	DOTtk, LPARENTHESIStk, RPARENTHESIStk, COMMAtk, LBRACEtk, RBRACEtk, 
	SEMICOLONtk, LBRACKETtk, RBRACKETtk, KEYWORDtk, ERRORtk, FALSEIDtk,
	NULLtk
};


struct token{ 
	tokenType tokenID;
	string tokenInstance;
	int lineNum;
	int scope;
};

#endif
