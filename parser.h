/* Emily Beauto
 * CS 4280
 * Project 4 Target ASM
 * 12/13/22
 * parser.h
 */

#ifndef PARSER_H
#define PARSER_H
#include <iostream>
#include "node.h"

node * parser(FILE *);

#endif

