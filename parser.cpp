/* Emily Beauto
 * CS 4280
 * Project 4 Target ASM
 * 12/13/22
 * parser.cpp
 */

#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <string>
#include "token.h"
#include "scanner.h"
#include "parser.h"
using namespace std;

static FILE * file;
token tk;
string tokenNames[]= {"EOFTk", "IDTk", "INTTk", "ASSIGNTk",
	"COMPARETk", "LESSTHANEQTk", "GREATERTHANEQTk",
	"COLONTk", "INCREMENTTk", "DECREMENTTk", "MULTIPLYTk",
	"DIVIDETk", "MODULUSTk", "DOTTk", "LPARENTHESISTk",
	"RPARENTHESISTk", "COMMATk",  "LBRACETk",
 	"RBRACETk", "SEMICOLONTk", "LBRACKETTk", "RBRACKETTk",  
	"KEYWORDTk", "ERROR", "FALSEID"
};

bool firstC;
char c;
int lineNumTotal;

string labelNames[] = {
	"PROGRAM", "BLOCK", "VARS", "EXP", "A", "N", "M", "R", "STATS", 
	"MSTAT", "STAT", "IN", "OUT", "IF", "LOOP", "ASSIGN", "RO"
};


node * createNode(nodeType l)
{
	node * root = new node;
	root-> label = l;

	token token1;
	token token2;
	token token3;
	token token4;
	token token5;
	root -> tk1 = token1;
	root -> tk2 = token2;
	root -> tk3 = token3;
	root -> tk4 = token4;
	root -> tk5 = token5;

	root -> child1 = NULL;
	root -> child2 = NULL;
	root -> child3 = NULL;
	root -> child4 = NULL;
	return root;

}

node * Program();
node * Block();
node * Vars();
node * Exp();
node * A();
node * N();
node * M();
node * R();
node * Stats();
node * Mstat();
node * Stat();
node * In();
node * Out();
node * If();
node * Loop();
node * Assign();
node * RO();


node * Program()
{
	node * node = createNode(PROGRAMn);
	node-> child1 = Vars();
	node-> child2 = Block();
	return node;
}


node * Block()
{
	node * node = createNode(BLOCKn);
	if(tk.tokenInstance == "begin")
	{
		node-> tk1 = tk;
		tk = scanner(firstC, c, lineNumTotal, file); 
		lineNumTotal = tk.lineNum;
		node-> child1 = Vars();
		node-> child2 = Stats();
		if(tk.tokenInstance == "end")
		{
			node-> tk2 = tk;
			tk = scanner(firstC, c, lineNumTotal, file); 
			lineNumTotal = tk.lineNum;
			return node;
		}	
		else
		{
			cout << "Error: Line " << tk.lineNum << " KEYWORDtk \"end\" expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
			exit(1); 
		}
	}
	else
	{
		cout << "Error: Line " << tk.lineNum << " KEYWORDtk \"begin\" expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
		exit(1);
	}
}


node * Vars()
{
	node * node = createNode(VARSn);
	if(tk.tokenInstance == "var")
	{
		node-> tk1 = tk;
		tk = scanner(firstC, c, lineNumTotal, file);
		lineNumTotal = tk.lineNum;
		if(tk.tokenID != IDtk)
		{
			cout << "Error: Line " << tk.lineNum << " IDtk expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
			exit(1);

		}
		node-> tk2 = tk;	
		tk = scanner(firstC, c, lineNumTotal, file);
		lineNumTotal = tk.lineNum;
		if(tk.tokenID != COLONtk)
		{
			cout << "Error: Line " << tk.lineNum << " :tk expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
			exit(1);
		}
		node-> tk3 = tk;
		tk = scanner(firstC, c, lineNumTotal, file);
		lineNumTotal = tk.lineNum;
		if(tk.tokenID != INTtk)
		{
			cout << "Error: Line " << tk.lineNum << " INTtk expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
			exit(1);
		}
		node-> tk4 = tk;
		tk = scanner(firstC, c, lineNumTotal, file);
		lineNumTotal = tk.lineNum;
		if(tk.tokenID != SEMICOLONtk)
		{
			cout << "Error: Line " << tk.lineNum << " ;tk expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
			exit(1);
		}
		node-> tk5 = tk;
		tk = scanner(firstC, c, lineNumTotal, file);
		lineNumTotal = tk.lineNum;
		node-> child1 = Vars();
		return node;
	}
	else
	{
		return node;
	}
}


node * Exp()
{
	node * node = createNode(EXPn);
	node-> child1 = A();
	if(tk.tokenID == INCREMENTtk)
	{
		node-> tk1 = tk;
		tk = scanner(firstC, c, lineNumTotal, file);
		lineNumTotal = tk.lineNum;
		node-> child2 = Exp();
		return node;
	}
	else
	{
		return node;	
	}
}


node * A()
{
	node * node = createNode(An);
	node-> child1 = N();
	if(tk.tokenID == DECREMENTtk)
	{
		node-> tk1 = tk;
		tk = scanner(firstC, c, lineNumTotal, file);
		lineNumTotal = tk.lineNum;
		node-> child2 = A();
		return node;
	}
	else
	{
		return node;
	}
}


node * N()
{
	node * node = createNode(Nn);
	node-> child1 = M();
	if(tk.tokenID == DIVIDEtk || tk.tokenID == MULTIPLYtk)
	{
		node-> tk1 = tk;
		tk = scanner(firstC, c, lineNumTotal, file);
		lineNumTotal = tk.lineNum;
		node-> child2 = N();
		return node;
	}
	else 
	{
		return node;
	}

}


node * M()
{
	node * node = createNode(Mn);
	if(tk.tokenID == DECREMENTtk)
	{
		node-> tk1 = tk;
		tk = scanner(firstC, c, lineNumTotal, file);
		lineNumTotal = tk.lineNum;
		node-> child1 = M();
		return node;
	}
	else
	{
		node-> child1 = R();
		return node;
	}
}


node * R()
{
	node * node = createNode(Rn);
	if(tk.tokenID == LBRACKETtk)
	{
		node-> tk1 = tk;
		tk = scanner(firstC, c, lineNumTotal, file);
		lineNumTotal = tk.lineNum;
		node->child1 = Exp();
		if(tk.tokenID == RBRACKETtk)
		{
			node-> tk2 = tk;
			tk = scanner(firstC, c, lineNumTotal, file);
			lineNumTotal = tk.lineNum;
			return node;
		}
		else
		{
			cout << "Error: Line " << tk.lineNum << " ]tk expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
			exit(1);
		}	
	}
	else if(tk.tokenID == IDtk || tk.tokenID == INTtk)
	{
		node-> tk1 = tk;
		tk = scanner(firstC, c, lineNumTotal, file);
		lineNumTotal = tk.lineNum;
		return node;
	}
	else
	{
		cout << "Error: Line " << tk.lineNum << " [tk/IDtk/INTtk expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
		exit(1);
	}
}


node * Stats()
{
	node * node = createNode(STATSn);
	node-> child1 = Stat();
	node-> child2 = Mstat();
	return node;
}


node * Mstat()
{
	node * node = createNode(MSTATn);
	if(tk.tokenInstance == "scan" || tk.tokenInstance == "print" || tk.tokenInstance == "fork" || tk.tokenInstance == "loop" || tk.tokenInstance == "begin" ||tk.tokenID == IDtk)
	{
		node-> child1 = Stat();
		node-> child2 = Mstat();
		return node;
	}
	else
	{
		return node;
	}
}


node * Stat()
{
	node * node = createNode(STATn);
	if(tk.tokenInstance == "scan")
	{
		node-> child1 = In();
		if(tk.tokenID == SEMICOLONtk)
		{
			node-> tk1 = tk;
			tk = scanner(firstC, c, lineNumTotal, file);
			lineNumTotal = tk.lineNum;
			return node;
		}
		else
		{
			cout << "Error: Line " << tk.lineNum << " ;tk expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
			exit(1);
		}	
	}	
	else if(tk.tokenInstance == "print")
	{
		node-> child1 = Out();
		if(tk.tokenID == SEMICOLONtk)
		{
			node-> tk1 = tk;
			tk = scanner(firstC, c, lineNumTotal, file);
			lineNumTotal = tk.lineNum;
			return node;
		}
		else
		{
			cout << "Error: Line " << tk.lineNum << " ;tk expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
			exit(1);
		}	
	}
	else if(tk.tokenInstance == "fork")
	{
		node-> child1 = If();
		if(tk.tokenID == SEMICOLONtk)
		{
			node-> tk1 = tk;
			tk = scanner(firstC, c, lineNumTotal, file);
			lineNumTotal = tk.lineNum;
			return node;
		}
		else
		{
			cout << "Error: Line " << tk.lineNum << " ;tk expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
			exit(1);
		}	
	}
	else if(tk.tokenInstance == "loop")
	{
		node-> child1 = Loop();
		if(tk.tokenID == SEMICOLONtk)
		{
			node-> tk1 = tk;
			tk = scanner(firstC, c, lineNumTotal, file);
			lineNumTotal = tk.lineNum;
			return node;
		}
		else
		{
			cout << "Error: Line " << tk.lineNum << " ;tk expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
			exit(1);
		}	
	}
	else if(tk.tokenID == IDtk)
	{
		node-> child1 = Assign();
		if(tk.tokenID == SEMICOLONtk)
		{
			node-> tk1 = tk;
			tk = scanner(firstC, c, lineNumTotal, file);	
			lineNumTotal = tk.lineNum;
			return node;
		}
		else
		{
			cout << "Error: Line " << tk.lineNum << " ;tk expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
			exit(1);
		}	
	}
	else if(tk.tokenInstance == "begin")
	{
		node-> child1 = Block();	
		return node;
	}
	else
	{
		cout << "Error: Line " << tk.lineNum << " KEYWORDtk/IDtk expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
		exit(1);
	}	
}


node * In()
{
	node * node = createNode(INn);
	if(tk.tokenInstance == "scan")
	{
		node-> tk1 = tk;
		tk = scanner(firstC, c, lineNumTotal, file);
		lineNumTotal = tk.lineNum;
		if(tk.tokenID == IDtk)
		{
			node-> tk2 = tk;
			tk = scanner(firstC, c, lineNumTotal, file);
			lineNumTotal = tk.lineNum;
			return node;
		}
		else
		{
			cout << "Error: Line " << tk.lineNum << " IDtk expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
			exit(1);
		}
	}
	cout << "Error: Line " << tk.lineNum << " KEYWORDtk \"scan\" expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
	exit(1);
}


node * Out()
{
	node * node = createNode(OUTn);
	if(tk.tokenInstance == "print")
	{
		node-> tk1 = tk;
		tk = scanner(firstC, c, lineNumTotal, file);
		lineNumTotal = tk.lineNum;
		if(tk.tokenID == LPARENTHESIStk)
		{
			node-> tk2 = tk;
			tk = scanner(firstC, c, lineNumTotal, file);
			lineNumTotal = tk.lineNum;
			node-> child1 = Exp();
			if(tk.tokenID == RPARENTHESIStk)
			{
				node-> tk3 = tk;
				tk = scanner(firstC, c, lineNumTotal, file);
				lineNumTotal = tk.lineNum;
				return node;
			}
			else
			{
				cout << "Error: Line " << tk.lineNum << " )tk expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
				exit(1);

			}
		}
		else
		{
			cout << "Error: Line " << tk.lineNum << " (tk expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
			exit(1);
		}	
	}
	else
	{
		cout << "Error: Line " << tk.lineNum << " KEYWORDtk \"print\" expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
		exit(1);
	}

}


node * If()
{
	node * node = createNode(IFn);
	if(tk.tokenInstance == "fork")
	{
		node-> tk1 = tk;
		tk = scanner(firstC, c, lineNumTotal, file);
		lineNumTotal = tk.lineNum;
		if(tk.tokenID == LPARENTHESIStk)
		{
			node-> tk2 = tk;
			tk = scanner(firstC, c, lineNumTotal, file);
			lineNumTotal = tk.lineNum;
			node-> child1 = Exp();
			node-> child2 = RO();
			node-> child3 = Exp();
			if(tk.tokenID == RPARENTHESIStk)
			{
				node-> tk3 = tk;
				tk = scanner(firstC, c, lineNumTotal, file);
				lineNumTotal = tk.lineNum;
				if(tk.tokenInstance == "then")
				{
					node-> tk4 = tk;
					tk = scanner(firstC, c, lineNumTotal, file);
					lineNumTotal = tk.lineNum;
					node->child4 = Stat();
					return node;
				}	
				else
				{
					cout << "Error: Line " << tk.lineNum << " KEYWORDtk \"then\" expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
					exit(1);
				}
			}
			else
			{
				cout << "Error: Line " << tk.lineNum << " )tk expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
				exit(1);
			}
		}
		else
		{
			cout << "Error: Line " << tk.lineNum << " (tk expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
			exit(1);
		}
	}
	else
	{
		cout << "Error: Line " << tk.lineNum << " KEYWORDtk \"fork\" expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
		exit(1);	
	}	
}


node * Loop()
{
	node * node = createNode(LOOPn);
	if(tk.tokenInstance == "loop")
	{
		node-> tk1 = tk;
		tk = scanner(firstC, c, lineNumTotal, file);
		lineNumTotal = tk.lineNum;
		if(tk.tokenID == LPARENTHESIStk)
		{
			node-> tk2 = tk;
			tk = scanner(firstC, c, lineNumTotal, file);
			lineNumTotal = tk.lineNum;
			node-> child1 = Exp();
			node-> child2 = RO();
			node-> child3 = Exp();
			if(tk.tokenID == RPARENTHESIStk)
			{
				node-> tk3 = tk;
				tk = scanner(firstC, c, lineNumTotal, file);
				lineNumTotal = tk.lineNum;
				node-> child4 = Stat();
				return node;
			}
			else
			{
				cout << "Error: Line " << tk.lineNum << " )tk expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
				exit(1);
			}
		}
		else
		{
			cout << "Error: Line " << tk.lineNum << " (tk expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
			exit(1);
		}
	}
	else
	{
		cout << "Error: Line " << tk.lineNum << " KEYWORDtk \"loop\" expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
		exit(1);	
	}	
}


node * Assign()
{
	node * node = createNode(ASSIGNn);
	if(tk.tokenID == IDtk)
	{
		node-> tk1 = tk;
		tk = scanner(firstC, c, lineNumTotal, file);
		lineNumTotal = tk.lineNum;
		if(tk.tokenID == COMPAREtk)
		{
			node-> tk2 = tk;
			tk = scanner(firstC, c, lineNumTotal, file);
			lineNumTotal = tk.lineNum;
			node-> child1 = Exp();
			return node;
		}
		else
		{
			cout << "Error: Line " << tk.lineNum << " COMPAREtk expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
			exit(1);
		}
	}
	else
	{
		cout << "Error: Line " << tk.lineNum << " IDtk expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
		exit(1);
	}
}


node * RO()
{
	node * node = createNode(ROn);
	if(tk.tokenID == LESSTHANEQtk || tk.tokenID == GREATERTHANEQtk || tk.tokenID == ASSIGNtk || tk.tokenID == MODULUStk)
	{
		node-> tk1 = tk;
		tk = scanner(firstC, c, lineNumTotal, file);
		lineNumTotal = tk.lineNum;
		return node;
	}
	else
	{
		cout << "Error: Line " << tk.lineNum << " RELATIONAL OPERATOR: ++/--/<=/>=/=/\% tk expected and instead tk \"" << tk.tokenInstance << "\" was recieved\n";
		exit(1);
	}
}


node * parser(FILE * filePtr)
{
	file = filePtr;	
	node *root;

	lineNumTotal = 1;
	firstC = true;
	tk = scanner(firstC, c, lineNumTotal, file); //tk =scanner()	
	lineNumTotal = tk.lineNum;
	firstC = false;
	
	root = Program();
	if(tk.tokenID == EOFtk)
	{
		return root;
	}
	else
	{
		cout << "Unsuccessful Parser\n";
		exit(1);
	}
}

