LOCAL OPTION, STORAGE = LOCAL OPTION
	Working Files: ALL FILES WORK 
	Not Working files: N/A

Emily Beauto
Project 4 ASM Target
CS 4280 Program Translation


howTo:
-first type "make" in text to execute the makefile
-To turn into .asm file: ./comp [file]

	-[file] is an optional argument. Implimented in two ways. Ex: ./comp code1 or ./comp < code1.cs4280
	-if no file argument is given program will use keyboard input till EOF (CTRL D). Program will 
	 take all user input till simulated EOF/crtl D, then execute tokens. Ex: ./comp

howTo: (run in virtualMachine)
-To run .asm file: ./VirtMach [file]

	-[file] is the .asm file just created with the ./comp.  Ex: ./VirtMach code1.asm 


Summary of Program:
	This program uses C++ and C to create a target asm file. This was built by a building on the previous
	Projects in CS 4280. For example, this uses a parser for the given BNF on Canvas using the Lexical Analyzer 
	from P1 which can distingush tokens without needing to be seperated by whitespace, except when needed to 
	prevent incorrect tokens. Additionally, if the parser is successful a test will be done to verify the 
	static semantics using local scope. If semantics is successful, you will then be given a target .asm file to 
	run on the virtual machine. If at any point this is unsucessfull you will recieve an error.

Eleven main files:
	1. p4.cpp : acts as the main function. Processes the arguments, creates the respective target file then calls the parser() and staticSem() functions.
	2. token.h : defines token structure. Holds the tokenID, tokenInstance, and lineNum.
	3. node.h: defines the node structure. Holds the label, tks, and children.
	4. staticSem.h : holds prototype for the staticSem.cpp
	5. staticSem.cpp : handles the check for static semantics including the verify, pop, and push functions. Also fills out asm code in the target.
	6. scanner.h  : holds prototypes for the scanner functions
	7. scanner.cpp : contains the heart of the lexical analyzer. Contains the FSA lookup table, filter and the findCol(). 
	8. parser.h : holds prototype for parser.cpp
	9. parser.cpp : implements the heart of the parser. Performs the BNF rules while storing tks and creating children of the nodes as necessary.
	10. makefile
	11. README.txt


Extra files:
	I have included extra files which are the tests that come from the P4 Testing page on Canvas.
	Please feel free to use these or create your own (vim XX.cs4280). 

