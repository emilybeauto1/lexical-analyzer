/* Emily Beauto
 * CS 4280
 * Project 4 Target ASM
 * 12/13/22
 * scanner.cpp
 */

#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <string>
#include "token.h"

using namespace std;

//based on matching chars to their ASCII value to figure out what column number for the FSA table
int findCol(char c)
{
	if(c >= 65 && c <= 90)
	{
		return 0;	//UPPERCASE A-Z
	}
	else if(c >= 97 && c <= 122){
		return 1;	//lowercase a-z
	}
	else if(c >= 48 && c <= 57){
		return 2;	//digits 0-9
	}
	else if(c == 61){
		return 3;	// =
	}
	else if(c == 60){
		return 4;	// <
	}
	else if(c == 62){
		return 5;	// >
	}
	else if(c == 58){
		return 6;	// :
	}
	else if(c == 43){
		return 7; 	// +
	}	
	else if(c == 45){
		return 8;	// - 
	}
	else if(c == 42){
		return 9; 	// *
	}
	else if(c == 47){
		return 10;	// /
	}
	else if(c == 37){
		return 11;	// %
	}
	else if(c == 46){
		return 12;	// .
	}
	else if(c == 40){
		return 13; 	// (
	}
	else if(c == 41){
		return 14; 	// )
	}
	else if(c == 44){
		return 15;	// ,
	}
	else if(c == 123){
		return 16;	// {
	}
	else if(c == 125){
		return 17;	// }
	}
	else if(c == 59){
		return 18; 	// ;
	}
	else if(c == 91){
		return 19;	// [
	}
	else if(c == 93){
		return 20; 	// ]
	}
	else if(c == EOF){
		return 21;	// EOF
	}
	else{
		return 22;	// extra characters
	}
}


//Filter does 3 tasks: 1 counts lines, 2 skip over spaces 3 skip over comments, it then focuses on getting the next char. Returns line count;
int filter(bool firstC, char &c, int lineNum, FILE *file)
{
	char ch;	//current char
	int line = 0;	//line counter for local scope
	
	//locate first char
	if(firstC == true)
	{
		ch = fgetc(file); 	//get char from file
	}
	else 
	{	
		ch = c; 	//pervious char
	}

	//while characters are "illegal" in alphabet + EOF handling -according to their in ASCII
	/*illegal values: ~ DEL    |    ^ _ '  backslash  @    ?    '    &    everything before % */
	while(ch == 126 || ch == 127 || ch == 124 || ch > 93 && ch < 97 || ch == 92 || 
		ch ==  64 || ch == 63 || ch == 39 || ch == 38 || ch < 37)
	{
		if(ch == '\n')	//filter task 1: counts line numbers
		{
			line++;
			ch = fgetc(file);
		}
		
		//changed to while loop after testing multiple spaces
		while(ch == ' ') //filter task 2: skip whitespace
		{
			ch = fgetc(file);
		}
		
		if(ch == 35) //filter task 3: skip comments
		{
			ch = fgetc(file);
			while(ch != EOF && ch != 35)	//keeps skipping chars till ending # is found
			{
				if(ch == '\n') //still keep track of line numbers
				{
					line++;
					ch = fgetc(file); //reimpliment task 1
				} 
				else //no new lines just get next character
				{
					ch = fgetc(file);
				}
			}
			if(ch == 35) //find last #, get next letter
			{
				ch = fgetc(file);
			}
			if(ch == EOF) //error for comment not closed
			{
				cout << "LEXICAL ERROR: comment was not closed before the EOF" << endl;
				exit(1);
			}
		}
		
		if(ch == EOF)
		{
			c = ch;
			return lineNum+line;
		}
		
		//explicted check for char in alphabet from Canvas suggestions
		if(ch != 10 &&  ch < 32 || ch > 32 && ch < 37 ||ch > 37 && ch < 40 
			|| ch == 63 || ch == 64 || ch == 92 || ch > 93 && ch < 97 
			|| ch == 124 || ch > 125)
		{
			cout << "LEXICAL ERROR: Line " << (lineNum + line) << ". Has an unidentified character: " << ch << endl;
			exit(1);
		}
	}
	c = ch;
	return lineNum + line;
}

//impliments FSA lookup table
token scanner(bool firstC, char &c, int line, FILE * file)
{

	//FSA lookup table
/*s*/	int FSAtable[27][23]= {
/*0*/	{26, 1, 2, 3, 5, 6, 9, 10, 12, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 1001, -2},
/*1*/	{1, 1, 1, 1002, 1002, 1002, 1002, 1002, 1002, 1002, 1002, 1002, 1002, 1002, 1002, 1002, 1002, 1002, 1002, 1002, 1002, 1002, 1002},
/*2*/	{1003, 1003, 2, 1003, 1003, 1003, 1003, 1003, 1003, 1003, 1003, 1003, 1003, 1003, 1003, 1003, 1003, 1003, 1003, 1003, 1003, 1003, 1003},
/*3*/	{1004, 1004, 1004, 4, 1004, 1004, 1004, 1004, 1004, 1004, 1004, 1004, 1004, 1004, 1004, 1004, 1004, 1004, 1004, 1004, 1004, 1004, 1004},
/*4*/	{1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005},
/*5*/	{-3, -3, -3, 7, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3},
/*6*/	{-4, -4, -4, 8, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4},
/*7*/	{1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006},
/*8*/	{1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007},
/*9*/	{1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008},
/*10*/	{-5, -5, -5, -5, -5, -5, -5, 11, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5},
/*11*/	{1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009},
/*12*/	{-6, -6, -6, -6, -6, -6, -6, -6, 13, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6},
/*13*/	{1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010},
/*14*/	{1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011},
/*15*/	{1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012},
/*16*/	{1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013},
/*17*/	{1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014},
/*18*/	{1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015},
/*19*/	{1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016},
/*20*/	{1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017},
/*21*/	{1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018},
/*22*/	{1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019},
/*23*/	{1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020},
/*24*/	{1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021},
/*25*/	{1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022}, 
/*26*/	{26, 26, 26, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}
	};

	//define keywords
	string keywords[14] = {"begin",  "end", "loop", "void", "var", "exit", "scan", "print", "main", "fork", "then", "let", "data", "func"};

	token t;
	//run token to filter to remove unnessary whitespace/comments and get line count.
	t.lineNum = filter(firstC, c, line, file);
	t.tokenInstance= ""; 
	
	//intial column value for FSA table/intial value
	int column = findCol(c);
	int value= FSAtable[0][column];
	//loop through FSAtable till final state or error.
	while(value >= 0 && value < 1000)
	{
		t.tokenInstance.push_back(c); //adds char to end of the string	
		c = fgetc(file);
		column = findCol(c);
		int extraSpace = FSAtable[value][column];
		value = extraSpace;
	}	
	
	//error messages: negative numbers in FSA Table
	if(value == -1)		//uppercase start tk causes FALSEIDtk
	{
		t.tokenID = FALSEIDtk;
		cout << "LEXICAL ERROR: Line " << t.lineNum << ". UPPERCASE letter used to start tk: " << t.tokenInstance << endl;
		exit(1);
	}
	if(value == -2)		//Undefined Char
	{
		t.tokenID = ERRORtk;
		cout << "LEXICAL ERROR: Line " << t.lineNum << ". Has an undefined character: " << c << endl;
		exit(1);
	}
	if(value == -3)		//invalid tk only <, need <= for valid tk
	{
		t.tokenID = ERRORtk;
		cout << "LEXICAL ERROR: Line " << t.lineNum << ". Invalid tk: only <, need <= for valid tk.\n"; 
		exit(1);
	}	
	if (value == -4)	//invalid tk only >, need >= for valid tk
	{
		t.tokenID = ERRORtk;
		cout << "LEXICAL ERROR: Line " << t.lineNum << ". Invalid tk: only >, need >= for valid >= tk.\n";
		exit(1);
	}
	if(value == -5)		//invalid tk only +, need ++ for valid tk
	{
		t.tokenID = ERRORtk;
		cout << "LEXICAL ERROR: Line " << t.lineNum << ". Invalid tk: only +, need ++ for valid tk.\n";
		exit(1);
	}
	if(value == -6)		//invalid tk only -, need -- for valid tk
	{
		t.tokenID = ERRORtk;
		cout << "LEXICAL ERROR: Line " << t.lineNum << ". Invalid tk: only -, need -- for valid tk.\n";
		exit(1);
	}


	//final states	
	//if id tk check for keywords
	bool flag = false;
	if(value == 1002)
	{
		for(int k = 0; k < 14; k++)
		{	
			if(t.tokenInstance == keywords[k])
			{
				flag = true; 	//key word found flag
				t.tokenID = KEYWORDtk;
				break; //leave loop if found
			}
		}
		if(!flag)
		{
			t.tokenID = IDtk; 
		}
	}

	//sets up other tk id values based on final state in FSA table
	if(value == 1001)	//eof
	{
		t.tokenID = EOFtk;
	}
	if(value ==  1003)	//int
	{	
		t.tokenID = INTtk;
	}
	if(value == 1004)	// =
	{
		t.tokenID = ASSIGNtk;
	}
	if(value == 1005)	// ==
	{
		t.tokenID = COMPAREtk;
	}
	if(value == 1006)	// <=
	{
		t.tokenID = LESSTHANEQtk;
	}
	if(value == 1007)	// >=
	{	
		t.tokenID = GREATERTHANEQtk;
	}
	if(value == 1008)	// :
	{
		t.tokenID = COLONtk;
	}
	if(value == 1009)	// ++
	{
		t.tokenID = INCREMENTtk;
	}
	if(value == 1010)	// --
	{
		t.tokenID = DECREMENTtk;
	}
	if(value == 1011)	// . 
	{
		t.tokenID = MULTIPLYtk;
	}
	if(value == 1012)	// /
	{
		t.tokenID = DIVIDEtk;
	}
	if(value == 1013)	// %
	{
		t.tokenID = MODULUStk;
	}
	if(value == 1014)	// .
	{
		t.tokenID = DOTtk;	
	}
	if(value == 1015)	// (
	{
		t.tokenID = LPARENTHESIStk;
	}
	if(value == 1016)	// )
	{
		t.tokenID = RPARENTHESIStk;
	}
	if(value == 1017)	// ,
	{
		t.tokenID = COMMAtk;
	}
	if(value == 1018)	// {
	{
		t.tokenID = LBRACEtk;
	}
	if(value == 1019)	// }
	{
		t.tokenID = RBRACEtk;
	}
	if(value == 1020)	// ;
	{
		t.tokenID = SEMICOLONtk;
	}
	if(value == 1021)	// [
	{
		t.tokenID = LBRACKETtk;
	}
	if(value == 1022)	// ]
	{
		t.tokenID = RBRACKETtk;
	}	
return t;
}

