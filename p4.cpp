/* Emily Beauto 
 * CS 4280
 * Project 4 Target ASM
 * 12/13/22
 * p4.cpp
 */

#include <iostream>
#include <stdio.h>
#include <string>
#include <fstream>
#include "token.h"
#include "parser.h"
#include "node.h"
#include "staticSem.h"
using namespace std;


int main(int argc, char *argv[])
{
	//if more than 2 arguements output error. Only have one optional file arguement
	if(argc > 2){
		cout << "Error: Expected an optional file arguement\n";
		return 1;
	}
	
	//define file/filename
	FILE *file = NULL;
	FILE *out = NULL;
	//fstream out;
	string fileName;
	string output;
	//if file arguement used
	if(argc == 2)
	{
		string ex = ".cs4280";    //append extentsion
		fileName = argv[1] + ex;
		string exOut = ".asm";
		output = argv[1] + exOut;
		file = fopen(fileName.c_str(), "r");
		//cout << fileName << endl;   //uncomment to see which file we are in	
		if(!file)
		{
			cout << "File not found. Failed to open " << fileName << " file\n";
			return 1;
		} 
	}
	else  //if file not used. Use keyboard input. Ex ./P1 < test.cs4280
	{
		//Runs input till simulated EOF with ctrl d
		fileName = "a";
		string oEx = ".asm";
		output = fileName + oEx;
		string word;
		ofstream ofile;	
		ofile.open(fileName.c_str());
		if(!ofile)
		{
			cout << "Failed to open file " << fileName << endl;
			return 1;
		}
		while(getline(cin, word))	//writes to file
		{
		//	cout << word << endl;
			ofile << word << endl;
		}
		ofile.close();	
		file= fopen(fileName.c_str(), "r");	//opens file for reading 
		if(!file)
		{
			cout << "Failed to open file " << fileName << endl;
			return 1;
		}
	
	}
	
	node *root = parser(file);

	out = fopen(output.c_str(), "w");
	if(!out)
	{	
		cout << "Target asm file creation error\n";
	}


	staticSem(root, 0, 0, out, output);
	cout << endl << output << endl;
	fclose(file);
	return 0;
}
