/* Emily Beauto
 * CS 4280
 * Project 4 Target ASM
 * 12/13/22
 * scanner.h 
 */


#ifndef SCANNER_H
#define SCANNER_H
#include <iostream>
#include "scanner.h"
#include "token.h"

int findCol(char c);
int filter(bool firstC, char &c, int lineNum, FILE * file);
token scanner(bool firstC, char &c, int lineNum, FILE * file);

#endif
