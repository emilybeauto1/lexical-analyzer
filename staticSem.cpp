/* Emily Beauto
 * CS 4280
 * Project 4 Target ASM
 * 12/13/22
 * staticSem.cpp
 */

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <cstdlib>
#include <string>
#include <sstream>
#include "staticSem.h"
#include "token.h"
using namespace std;
//cout comments are within file but commented out for ease of reading whats happening

//declare vars
token stack[100];
int countTotal =0;
static int scope = 0;

//storing the tempvars to later print.
static string temp_vars[100];
static int LabelCntr=0;
static int VarCntr=0;
typedef enum {VAR, LABEL} nameType;

static string newName(nameType what)
{ 
  	string Name ="";
  	stringstream ss ; 
	
	//Name = "T" +ss.str();
	if (what==VAR) // creating new temporary
    	{
		ss << VarCntr;
		Name = "T" +ss.str(); /* generate a new label as T0, T1, etc */
		VarCntr++;
	}
	else // creating new Label
	{
		ss << LabelCntr;
		Name = "L" + ss.str(); /* new lables as L0, L1, etc */
		LabelCntr++;
	}
	return Name;
}





//verify tk
int verify(token tk)
{
	//cout << "verify tk: " << tk.tokenInstance << " Line: " << tk.lineNum << endl;
	if(countTotal < 1)
	{
		cout << "Verify: can't verify on an empty stack" << endl;
		exit(1);
	}
	for(int i = countTotal - 1; i>= 0; i--)
	{
		if(stack[i].tokenInstance == tk.tokenInstance)
		{
	//		cout << "Found: " << tk.tokenInstance << ", distance = " << countTotal -i -1 << endl;
			return countTotal -i -1;
		}
	}
	return -1;
}

//create pop function
void pop(int countScope, FILE *out)
{
	if(countTotal!=0)
	{
//	cout << "Pop countScope " << countScope << endl;
//	cout << "Total = " << countTotal << endl;
	for(int i = 0; i < 100; i++)
	{
		if(stack[i].scope == countScope)
		{
//			cout << "Popping " << stack[i].tokenInstance << endl;
			fprintf(out, "POP\n");
			stack[i].tokenInstance = "";
			countTotal--;
			if(countTotal < 1)
			{
				break;
			}
		}
	}
	}
}


//create push function
void push(token tk, int sCount, FILE* out, string output)
{
	if(countTotal < 100)
	{
//		cout << "PUSH: " << tk.tokenInstance << " Line: " << tk.lineNum <<  endl;
		countTotal++;
//		cout <<  "sCount= " << sCount << " countTotal = " << countTotal << endl;
		tk.scope = sCount;
		for(int i =0; i<100; i++)
		{
			if(stack[i].tokenInstance == tk.tokenInstance)
			{	
				if(stack[i].scope == tk.scope)
				{
					cout << "Error: Variable \"" << tk.tokenInstance << "\" on line " << tk.lineNum << " was already declared previously in this scope at line " << stack[i].lineNum << ".\n";
					remove(output.c_str());
					exit(1);
				}
			}
			if(stack[i].tokenInstance == "")
			{
				stack[i] = tk;
				break;
			}
		}
		fprintf(out, "PUSH\n");	
	}
	else 
	{
		cout << "Error: Overflow because there are too many variable" << endl;
		remove(output.c_str());
		exit(1);
	}
} 

//create check for each case in BNF
void staticSem(node * n, int countScope, int varCount, FILE *out, string output)
{
	if(n == NULL)
	{
		return;
	}
	switch(n-> label){
		case PROGRAMn:
			//cout << "PROGRAM\n";
			countScope = 0;
			staticSem(n-> child1, countScope, varCount, out, output);
			staticSem(n-> child2, countScope, varCount, out, output);
			pop(0, out);
			fprintf(out, "STOP\n");
			for(int i  = 0; i < VarCntr; i++)
			{
				fprintf(out, "T%d 0\n", (VarCntr-i-1));
			}	 	
			return;
		case BLOCKn:
			//cout << "BLOCK\n";
			countScope++;
			varCount = 0;
			staticSem(n-> child1, countScope, varCount, out, output);
			staticSem(n-> child2, countScope, varCount, out, output);
			pop(countScope, out);
			countScope--;
			return;
		case VARSn:
			//cout<< "VARS\n";
			if(n->tk2.tokenInstance != "")
			{
				varCount++;
//				cout << n->tk2.tokenInstance << " found, varCount = " << varCount << endl;
				push(n->tk2, countScope, out, output);
//				cout << "varCount = " << varCount << endl;
				fprintf(out, "LOAD %s\n", n->tk4.tokenInstance.c_str());
				fprintf(out, "STACKW 0\n");
				staticSem(n-> child1, countScope, varCount, out, output);
			}
			return;
		case EXPn:
			//cout << "EXP\n";
			if(n-> tk1.tokenID == INCREMENTtk)
			{	
				staticSem(n->child2, countScope, varCount, out, output);
				string tempVar = newName(VAR);
				fprintf(out, "STORE %s\n", tempVar.c_str());
				staticSem(n->child1, countScope, varCount, out, output);
				fprintf(out , "ADD %s\n", tempVar.c_str());
			}		
			else
			{
				staticSem(n->child1, countScope, varCount, out, output);
			}
			return;
		case An:
			//cout << "A\n";
			if(n-> tk1.tokenID == DECREMENTtk)
			{
				staticSem(n->child2, countScope, varCount, out, output);
				string tempVar = newName(VAR);
				fprintf(out, "STORE %s\n", tempVar.c_str());
				staticSem(n->child1, countScope, varCount, out, output);
				fprintf(out, "SUB %s\n", tempVar.c_str());	
			}
			else
			{	
				staticSem(n->child1, countScope, varCount, out, output);
			}
			return;
		case Nn:
			//cout << "N\n";
			if(n->tk1.tokenID == DIVIDEtk)
			{
				staticSem(n->child2, countScope, varCount, out, output);
				string tempVar = newName(VAR);
				fprintf(out, "STORE %s\n", tempVar.c_str());
				staticSem(n->child1, countScope, varCount, out, output);
				fprintf(out, "DIV %s\n",tempVar.c_str()); 
			}
			else if(n->tk1.tokenID == MULTIPLYtk)	
			{
				staticSem(n->child2, countScope, varCount, out, output);
				string tempVar = newName(VAR);
				fprintf(out, "STORE %s\n", tempVar.c_str());
				staticSem(n->child1, countScope, varCount, out, output);
				fprintf(out, "MULT %s\n", tempVar.c_str());
			}
			else
			{
				 staticSem(n->child1, countScope, varCount, out, output);
			}	
			return;
		case Mn:
			//cout << "M\n";
			staticSem(n->child1, countScope, varCount, out, output);	
			if(n-> tk1.tokenID == DECREMENTtk)
			{
				fprintf(out, "MULT -1\n");	
			}	
			return;
		case Rn:
			//cout << "R\n";
			if(n-> tk1.tokenID == LBRACKETtk)
			{
				staticSem(n->child1, countScope, varCount, out, output);
			}
			if(n->tk1.tokenID == IDtk)
			{
//				cout << "found IDtk: " << n->tk1.tokenInstance << endl;
				if(n->tk1.tokenInstance != "")
				{
					if(verify(n-> tk1) < 0)
					{
						cout << "ERROR in R: Error in verifying tk \"" << n->tk1.tokenInstance << "\" at line " << n->tk1.lineNum << ".\n";
						remove(output.c_str());
						exit(1);
					}
				}
				fprintf(out, "STACKR %d\n", verify(n->tk1));
			}
			if(n->tk1.tokenID == INTtk)
			{
				 fprintf(out, "LOAD %s\n", n->tk1.tokenInstance.c_str());
			}
			return;
		case STATSn:
			//cout << "STATS\n";
			staticSem(n->child1, countScope, varCount, out, output);
			staticSem(n->child2, countScope, varCount, out, output);
			return;
		case MSTATn:
			//cout << "MSTAT\n";
			staticSem(n->child1, countScope, varCount, out, output);
			staticSem(n->child2, countScope, varCount, out, output);
			return;
		case STATn:
			//cout << "STAT\n";
			staticSem(n->child1, countScope, varCount, out, output);
			return;
		case INn:
		{
			//cout << "IN\n";
			if(verify(n->tk2) < 0)
			{
				cout << "ERROR in IN: Error in verifying tk \"" << n->tk2.tokenInstance << "\" at line " << n->tk2.lineNum << ".\n";	
				remove(output.c_str());
				exit(1);
			}
//storing tk2 instance
			string tempVar = newName(VAR);
			fprintf(out, "READ %s\n", tempVar.c_str());		
			fprintf(out, "LOAD %s\n", tempVar.c_str());
			fprintf(out, "STACKW %d\n", verify(n->tk2));
			return;
		}
		case OUTn:
		{	//cout << "OUT\n";
			string tempVar1 = newName(VAR);
			staticSem(n->child1, countScope, varCount, out, output);
			fprintf(out, "STORE %s\n", tempVar1.c_str());
			fprintf(out, "WRITE %s\n", tempVar1.c_str());
			return;
		}
		case IFn:
		{
			//cout << "IF\n";
			staticSem(n->child3, countScope, varCount, out, output);
			string tempVar = newName(VAR);
			fprintf(out, "STORE %s\n", tempVar.c_str());		
			staticSem(n->child1, countScope, varCount, out, output);
			fprintf(out, "SUB %s\n", tempVar.c_str());
			string tempVar1 = newName(LABEL);
			if(n->child2->tk1.tokenID == LESSTHANEQtk)
			{
				fprintf(out, "BRPOS %s\n", tempVar1.c_str());
			}
			else if (n->child2->tk1.tokenID == MODULUStk)
			{
				fprintf(out, "BRZERO %s\n", tempVar1.c_str());
			} 
			else if (n->child2-> tk1.tokenID == GREATERTHANEQtk)
			{
				fprintf(out, "BRNEG %s\n", tempVar1.c_str());
			}
			else //=
			{
				fprintf(out, "BRPOS %s\n", tempVar1.c_str());
				fprintf(out, "BRNEG %s\n", tempVar1.c_str());
			}
			staticSem(n->child4, countScope, varCount, out, output);
			fprintf(out,"%s: NOOP\n", tempVar1.c_str());
			return;
		}
		case LOOPn:
		{	//cout << "LOOP\n";
			string tempLabel = newName(LABEL);
			string tempLabel2 = newName(LABEL);
			string tempVar = newName(VAR);
			fprintf(out, "%s: NOOP\n", tempLabel.c_str());
			staticSem(n->child3, countScope, varCount, out, output);
			fprintf(out, "STORE %s\n", tempVar.c_str());
			staticSem(n->child1, countScope, varCount, out, output);
			fprintf(out, "SUB %s\n", tempVar.c_str());	
			if(n->child2->tk1.tokenID == LESSTHANEQtk)
			{
				fprintf(out, "BRPOS %s\n", tempLabel2.c_str());
			}
			else if (n->child2->tk1.tokenID == MODULUStk)
			{
				fprintf(out, "BRZERO %s\n", tempLabel2.c_str());
			}
			else if (n->child2-> tk1.tokenID == GREATERTHANEQtk)
			{
				fprintf(out, "BRNEG %s\n", tempLabel2.c_str());
			}
			else
			{
				fprintf(out, "BRPOS %s\n", tempLabel2.c_str());
				fprintf(out, "BRNEG %s\n", tempLabel2.c_str());
			}
			staticSem(n->child4, countScope, varCount, out, output);
			fprintf(out, "BR %s\n", tempLabel.c_str());
			fprintf(out,"%s: NOOP\n", tempLabel2.c_str());
			return;
		}
		case ASSIGNn:
			if(verify(n-> tk1) < 0)
			{
				cout << "ERROR in ASSIGN: Error in verifying tk \""     << n->tk1.tokenInstance << "\" at line " << n->tk1.lineNum << ".\n";
				remove(output.c_str());
				exit(1);
			}
			staticSem(n->child1, countScope, varCount, out, output);
			fprintf(out, "STACKW %d\n", verify(n->tk1));
			return;
		case ROn:
		{
			return;
		}
		default:
			cout << "ERROR: In default\n";
			remove(output.c_str());
			exit(1);
	}
}
